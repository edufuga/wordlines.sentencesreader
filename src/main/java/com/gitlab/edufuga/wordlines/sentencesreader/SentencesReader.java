package com.gitlab.edufuga.wordlines.sentencesreader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class SentencesReader {
    private File sentencesFolder;

    public SentencesReader(File folder) {
        if (!folder.exists()) {
            System.out.println("The given sentences folder " + folder + " does not exist.");
            return;
        }

        if (!folder.isDirectory()) {
            System.out.println("This is not a sentences folder.");
            return;
        }

        this.sentencesFolder = folder;
    }

    public List<String> getSentencesForWord(String word) throws IOException {
        String path = sentencesFolder.getAbsolutePath() + "/" + word + ".txt";
        File wordFileWithSentences = new File(path);
        if (!wordFileWithSentences.exists()) {
            System.out.println("The sentences file for the word '" + word + "' doesn't exist.");
            return Collections.emptyList();
        }

        return Files.readAllLines(wordFileWithSentences.toPath());
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("Give me the file path and the word to read the sentences for.");
            return;
        }

        String filePathString = args[0];
        if (filePathString == null || filePathString.isEmpty()) {
            System.out.println("The file path isn't given.");
            return;
        }

        String word = args[1];
        if (word == null || word.isEmpty()) {
            System.out.println("The word isn't given.");
            return;
        }

        Path filePath = Paths.get(filePathString);

        if (Files.notExists(filePath)) {
            System.out.println("The file path doesn't exist.");
            return;
        }

        SentencesReader sentencesReader = new SentencesReader(filePath.toFile());
        List<String> sentences = sentencesReader.getSentencesForWord(word);
        sentences.forEach(System.out::println);
    }
}
